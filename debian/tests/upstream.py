import sys
import itertools
import pytest

blacklist = ["test_knn.py", # Requires pykeops
             "test_representations.py", # Look into this
             "test_wasserstein_distance.py::test_wasserstein_distance_grad", # Requires PyTorch.
             "test_remote_datasets.py" # Requires Internet access, and downloads DFSG-dubious data.
            ]

print("Blacklist: %s" %(str(blacklist)))

# These should be completely ignored, not even loaded (their imports fail).
ignorelist = ["test_diff.py", # Requires TF.
              "test_dtm.py", # Requires hnswlib.
              "test_perslay.py", # Requires TF
              "test_wasserstein_with_tensors.py" # Requires PyTorch and TF
            ]

print("Ignorelist:", ignorelist)

testdir = sys.argv[1]

deselect_args = list(itertools.chain.from_iterable([["--deselect", "%s/%s" %(testdir, exclude)] for exclude in blacklist]))
ignore_args = list(itertools.chain.from_iterable([["--ignore-glob", "%s/%s" %(testdir, exclude)] for exclude in ignorelist]))

exitcode = pytest.main([testdir] + deselect_args + ignore_args)
exit(exitcode)
