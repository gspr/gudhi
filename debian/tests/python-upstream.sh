#!/bin/bash

set -e
set -u

helper=$PWD/debian/tests/upstream.py
testdir=$PWD/src/python/test
cp $helper $AUTOPKGTEST_TMP
cp -R $testdir $AUTOPKGTEST_TMP
cd $AUTOPKGTEST_TMP

rm -f test/test_dtm.py # Imports torch too early.
rm -f test/test_wasserstein_distance.py # Skip tests relying on Hera to allow package migration despite of bug #1089128

for py3ver in $(py3versions -vs)
do
    echo "Running tests with Python ${py3ver}."
    /usr/bin/python${py3ver} -B upstream.py test
    rm -rf .pytest_cache
    echo "**********"
done
